# Ibis

The standard library of the Thoth operating system

---

## What is Thoth?

Thoth is a general-purpose, hybrid, UNIX-inspired (but not UNIX-like) operating system designed as an experiment into operating system design and development. Thoth aims to be a hybrid system and is designed such that kernel-level components such as drivers and generic servers may be loaded and unloaded into the system at run-time. As of October 2016, this design goal is unimplemented.

## Design Goals

* UNIX-inspired environment
	* Abstract I/O implemented through filesystem
* Modular kernel structure
* Drivers for most common hardware implemented

## Who develops Thoth?

Thoth is currently primarily developed by me, Joshua Barretto (<joshua.s.barretto@gmail.com>)

---

## What is Ibis?

Ibis is the standard library for the Thoth operating system. It has user-land and kernel-land implementations. As a result, this repository only contains the header definitions for Ibis. Ibis is intended as a replacement or complement to the C++ standard library and includes things like generic data structures, useful structures like managed strings, operating system interfacing code and commonly used mathematic and logic functions.

## Who maintains Ibis?

Ibis is currently maintained by me, Joshua Barretto (<joshua.s.barretto@gmail.com>)
