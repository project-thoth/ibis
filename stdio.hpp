/*
* 	filename  : stdio.hpp
* 	component : ibis
*
* 	This file is part of Thoth.
*
* 	Thoth is free software: you can redistribute it and/or modify
* 	it under the terms of the GNU General Public License as published by
* 	the Free Software Foundation, either version 3 of the License, or
* 	(at your option) any later version.
*
* 	Thoth is distributed in the hope that it will be useful,
* 	but WITHOUT ANY WARRANTY; without even the implied warranty of
* 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* 	GNU General Public License for more details.
*
* 	You should have received a copy of the GNU General Public License
* 	along with Thoth.  If not, see <http://www.gnu.org/licenses/>.
*/

// Begin header guard
#ifndef _IBIS_STDIO_HPP
#define _IBIS_STDIO_HPP

// Ibis headers
#include <ibis/string.hpp>
#include <ibis/value.hpp>

namespace ibis
{
	enum class file_msg_op
	{
		DEFAULT    = 1,
		CLEAR      = 2,
		FORECOLOUR = 3,
		BACKCOLOUR = 4,
	};

	struct file_msg
	{
		unsigned int _op;
		byte* _data;
		memint _size;

		file_msg(byte* _data, memint _size, unsigned int _op = (unsigned int)file_msg_op::DEFAULT)
		{
			this->_data = _data;
			this->_size = _size;
			this->_op = _op;
		}

		unsigned int op() const { return this->_op; }
		byte*      data() const { return this->_data; }
		memint     size() const { return this->_size; }
	};

	struct file
	{
	private:

		unsigned long _id;

	public:

		file(unsigned long _id = 0)
		{
			this->_id = _id;
		}

		unsigned long id() { return this->_id; }
		bool valid() { return this->_id != 0; }

		status write(file_msg _msg);
		status close();
	};

	file open(ibis::string _path);

	status print(string _str);
	status print_line(string _str);
	status print_format(string _fmt, ...);
}

// End header guard
#endif
