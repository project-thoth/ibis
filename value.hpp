/*
* 	filename  : value.hpp
* 	component : ibis
*
* 	This file is part of Thoth.
*
* 	Thoth is free software: you can redistribute it and/or modify
* 	it under the terms of the GNU General Public License as published by
* 	the Free Software Foundation, either version 3 of the License, or
* 	(at your option) any later version.
*
* 	Thoth is distributed in the hope that it will be useful,
* 	but WITHOUT ANY WARRANTY; without even the implied warranty of
* 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* 	GNU General Public License for more details.
*
* 	You should have received a copy of the GNU General Public License
* 	along with Thoth.  If not, see <http://www.gnu.org/licenses/>.
*/

// Open header guard
#ifndef _IBIS_VALUE_HPP
#define _IBIS_VALUE_HPP

// Begin headers
#include <ibis/types.hpp>

namespace ibis
{
	/* Status enum */

	enum class result { SUCCESS = 0, FAILURE = 1, };

	/* Status type */

	struct status
	{
		int _err;

		status(int _err)
		{
			this->_err = _err;
		}

		status(result _err)
		{
			this->_err = (int)_err;
		}

		bool success() { return this->_err == (int)result::SUCCESS; }
		int error() { return this->_err; }
	};

	/* Result type */

	template <typename T>
	struct value : public status
	{
		T _data;

		value(T _data, int _err) : status(_err)
		{
			this->_data = _data;
		}

		value(T _data, result _err) : status(_err)
		{
			this->_data = _data;
		}

		T data() { return this->_data; }
	};
}

// End header guard
#endif
